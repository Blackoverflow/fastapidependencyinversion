import unittest

from users_functional import all_users


class AllUsersTestCase(unittest.TestCase):
    """
    Testing the logic is possible without additional dependencies or complicated setup
    """

    def test_all_users_returns_dummy_data(self):
        users = all_users()
        self.assertEqual(len(users), 2)
        self.assertEqual(users[0]["name"], "Alice")
        self.assertEqual(users[1]["name"], "Bob")


if __name__ == "__main__":
    unittest.main()
