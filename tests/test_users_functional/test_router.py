import unittest

from tests.client import router_test_client
from users_functional.route import users_router

dummy_response = ["hello world"]


def all_users():
    return dummy_response


class RouterTestCase(unittest.TestCase):
    """
    Tests for framework dependant code can focus on technical aspects, using dummy implementations for dependencies.
    """

    def setUp(self):
        def dependency():
            return all_users

        self.client = router_test_client(users_router(dependency))

    def test_users_route(self):
        response = self.client.get("/users")
        self.assertEqual(200, response.status_code)
        self.assertListEqual(response.json(), dummy_response)


if __name__ == "__main__":
    unittest.main()
