from fastapi import FastAPI
from fastapi.testclient import TestClient


def router_test_client(router):
    app = FastAPI()
    app.include_router(router)

    return TestClient(app)
