import unittest

from users_oop import Users


class UsersTestCase(unittest.TestCase):
    """
    Testing the logic is possible without additional dependencies or complicated setup
    """

    def test_all_users_returns_dummy_data(self):
        users = Users().all_users()
        self.assertEqual(len(users), 2)
        self.assertEqual(users[0]["name"], "Alice")
        self.assertEqual(users[1]["name"], "Bob")


if __name__ == "__main__":
    unittest.main()
