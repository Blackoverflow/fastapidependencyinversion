import unittest

from tests.client import router_test_client
from users_oop.route import users_router

dummy_response = ["hello world"]


class DummyUsers:

    def __init__(self, response):
        self.response = response

    def all_users(self):
        return self.response


class RouterTestCase(unittest.TestCase):
    """
    Tests for framework dependant code can focus on technical aspects, using dummy implementations for dependencies.
    """

    def setUp(self):
        def users_dependency():
            return DummyUsers(dummy_response)

        self.client = router_test_client(users_router(users_dependency))

    def test_users_route(self):
        response = self.client.get("/users")
        self.assertEqual(200, response.status_code)
        self.assertListEqual(response.json(), dummy_response)


if __name__ == "__main__":
    unittest.main()
