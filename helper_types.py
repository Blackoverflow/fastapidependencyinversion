from typing import Callable, TypeVar

R = TypeVar("R")
Supplier = Callable[[], R]
