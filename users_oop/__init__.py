from .route import users_router
from .users import Users

"""
    Integration of the dummy implementation and the router
"""


def users_dependency():
    return Users()


router = users_router(users_dependency)
