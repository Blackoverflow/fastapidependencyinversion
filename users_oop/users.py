from typing import List


class Users:
    """
    Dummy implementation of the dependency
    """

    def __init__(self):
        self._users = [{"name": "Alice"}, {"name": "Bob"}]

    def all_users(self) -> List[dict]:
        return self._users.copy()
