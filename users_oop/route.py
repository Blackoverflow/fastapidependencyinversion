from typing import List

from fastapi import APIRouter, Depends
from typing_extensions import Annotated, Protocol

from helper_types import Supplier


class UserSource(Protocol):
    """
    Definition of the dependency
    compatible with duck typing
    """

    def all_users(self) -> List[dict]: ...


def users_router(user_source_dependency: Supplier[UserSource]):
    """
        Definition of routes with inverted dependency as object

    :param user_source_dependency: provide the dependency implementing the Protocol :class:`users_oop.route.UserSource`
    of this router here.
        FastAPI wants a closure in order to separate requests.

    :return: FastAPI Router
    """
    router = APIRouter()

    @router.get("/users/", tags=["user"])
    async def read_users(
        source: Annotated[UserSource, Depends(user_source_dependency)]
    ):
        return source.all_users()

    return router
