from typing import List

from fastapi import APIRouter, Depends
from typing_extensions import Annotated

from helper_types import Supplier

AllUsers = Supplier[List[str]]


def users_router(user_source: Supplier[AllUsers]):
    """
        Definition of routes with inverted dependency as function

    :param user_source: provide the dependency compatible with :class:`users_functional.route.AllUsers`
    of this router here.
        FastAPI wants a closure in order to separate requests.

    :return: FastAPI Router
    """
    router = APIRouter()

    @router.get("/users/", tags=["user"])
    async def read_users(all_users: Annotated[AllUsers, Depends(user_source)]):
        return all_users()

    return router
