from .route import users_router
from .users import all_users

"""
    Integration of the dummy implementation and the router
"""


def all_users_dependency():
    """Closure, which is called for each handled request"""
    return all_users


router = users_router(all_users_dependency)
