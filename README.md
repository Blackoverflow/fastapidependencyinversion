# FastAPI Dependeny Inversion

This is a simple example of a way to do dependency inversion with FastAPI (https://fastapi.tiangolo.com/).

## Requirements

Run `./install_requirements.sh` to install all requirements into a Virtual Environment.

## Test

Run `./run_tests.sh` to run tests.

They are defined in `./tests/`.

## Run

Run `./serve.sh` to run a server on http://127.0.0.1:8000.

Run `./requests.sh` to make requests against a running server.

## Examples

Two examples that highlight how dependency inversion is possible with FastApi.

This allows for separation of concerns which makes testing and code comprehension a lot easier.

Both examples use type hinting.

### Users Functional

This example uses Functional Programming in order to provide a function
`users_functional.users.all_users()` which is called upon request on
http://127.0.0.1:8000/functional/users.

A different function is used while testing.

The integration is done in `./users_functional/__init__.py`.

### Users OOP

This example uses Object-Oriented Programming in order to provide a class
`users_oop.users.Users` with a method `all_users()` which is
called upon request on http://127.0.0.1:8000/oop/users.

A different class is used while testing.

The integration is done in `./users_oop/__init__.py`. 