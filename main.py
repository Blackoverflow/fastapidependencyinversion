from fastapi import FastAPI

import users_functional
import users_oop

app = FastAPI()

app.include_router(users_functional.router, prefix="/functional")
app.include_router(users_oop.router, prefix="/oop")
# The integrated router can be used like any other router
