#!/bin/bash
if [ ! -d ./venv ]; then
  echo "run 'install_requirements.sh' first"
  exit
fi

venv/bin/uvicorn main:app --reload